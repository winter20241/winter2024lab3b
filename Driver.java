import java.util.Scanner;
public class Driver {
	public static void main (String [] args) {
		Scanner reader= new Scanner(System.in); 
		Dinosaur gaggle [] = new Dinosaur [4];
		for (int i = 0; i<gaggle.length; i++) {
			System.out.println("Enter a name, height, and coolness of a dinosaur");
			String name = reader.next();
			double height = reader.nextDouble();
			int coolness = reader.nextInt();
			
			gaggle[i] = new Dinosaur(name, height, coolness);
		}
		
		System.out.println(gaggle[gaggle.length-1].name);
		System.out.println(gaggle[gaggle.length-1].height);
		System.out.println(gaggle[gaggle.length-1].coolness);
		
		gaggle[0].Greeting();
		gaggle[0].HowManyHumansTall();
	}
}